import React from 'react';
import ReactDOM from 'react-dom';

export default class RenderSceneOnBody extends React.Component {
  constructor(props) {
    super(props);
    this.scene = document.createElement('a-scene');
    this.scene.setAttribute('vr-mode-ui', 'enabled: false;');
    this.scene.setAttribute(
      'renderer',
      'logarithmicDepthBuffer: true; sortObjects: true;'
    );
    this.scene.setAttribute('embedded', 'true');
    this.scene.setAttribute(
      'arjs',
      'trackingMethod: best; sourceType: webcam; sourceWidth: 1280; sourceHeight: 960; displayWidth: 1280; displayHeight: 960;'
    );
  }

  componentDidMount() {
    document.body.prepend(this.scene);
  }

  componentWillUnmount() {
    document.body.removeChild(this.scene);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.scene);
  }
}
