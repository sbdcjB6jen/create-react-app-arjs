import styled from 'styled-components';

export const NavStyles = styled.nav`
  position: fixed;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: center;
  top: 0px;
  left: 0;
  width: 100vw;
  z-index: 1;
  padding: 5px 0;
  background-color: #5a96cfc2;
  color: #fff;
  font-size: 1.5rem;
`;
export const NavItemStyles = styled.div`
  padding: 10px 5px;
`;
