import RenderSceneOnBody from './components/RenderSceneOnBody';
import { NavStyles, NavItemStyles } from './components/styles/NavStyles';
import ABox from './components/ABox';
import AEntity from './components/AEntity';

// some dummy data that could be pulled from an API

const data = {
  corsProxy: 'https://immense-refuge-77873.herokuapp.com/',
  patternURL: 'https://home.jonnypage.ca/iversoft/pattern/pattern-01.patt',
  box: {
    color: 'yellow',
    position: { x: 0, y: 0, z: 0 },
    scale: { x: 1, y: 1, z: 1 },
  },
  cone: {
    color: 'green',
    position: { x: 0, y: 1, z: 0 },
    scale: { x: 0.5, y: 1, z: 0.5 },
  },
};

function App() {
  return (
    <div className="app">
      <RenderSceneOnBody>
        {/* dynamic pattern loading: url={`${data.corsProxy}${data.patternURL}`}>*/}
        <a-marker type="pattern" url="pattern/pattern-01.patt">
          <ABox color={data.box.color} scale={data.box.scale}></ABox>
          <AEntity
            geometry="primitive: cone"
            color={data.cone.color}
            position={data.cone.position}
            scale={data.cone.scale}
          ></AEntity>
        </a-marker>
        <a-entity camera></a-entity>
      </RenderSceneOnBody>

      <NavStyles>
        <NavItemStyles>Augmented Reality</NavItemStyles>
        <NavItemStyles>About</NavItemStyles>
      </NavStyles>
    </div>
  );
}

export default App;
